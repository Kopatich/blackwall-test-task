import React from 'react';
import styled, { css } from 'styled-components';
import Dropdown from 'components/DependentDropdown/DependentDropdown';
import { FILTER_TYPES } from './DependentPanel.const';
import { usePanel } from './DependentPanel.hooks';

const StyledPanel = styled.div`
    border: 1px solid red;
    width: 500px;

    display: flex;
    flex-direction: column;
`;

const StyledPanelHeader = styled.h3`
    color: rgb(184, 81, 81);
`;

const StyledFiltersContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
`;

interface StyledFilterProps {
    isSelected: boolean
}

const StyledFilter = styled.div <StyledFilterProps>`
    padding: 10px;
    border-radius: 10px;
    font-weight: bold;
    cursor: pointer;
    transition: 0.2s;
    
    &:hover {
        background-color: rgba(184, 81, 81, 0.4);
    }

    ${({ isSelected }) => {
    if (isSelected) {
            return css`
                color: white;
                background-color: rgb(184, 81, 81);

                &:hover {
                    background-color: rgb(184, 81, 81);
                }
            `;
        }
    }}
`;

const DependentPanel = () => {
    const { selectedFilter, filterNames, setselectedFilter } = usePanel();

    return (
        <StyledPanel>
            <StyledPanelHeader>Получаете</StyledPanelHeader>
            <StyledFiltersContainer>
                {filterNames.map(([filterCode, filterInfo]) => (
                    <StyledFilter
                        isSelected={selectedFilter === filterCode}
                        key={filterCode}
                        onClick={() => {
                            setselectedFilter(filterCode as FILTER_TYPES);
                        }}
                    >
                        {filterInfo.name}
                    </StyledFilter>
                ))}
            </StyledFiltersContainer>
            <Dropdown selectedFilter={selectedFilter} filterNames={filterNames} />
        </StyledPanel>
    );
};

export default DependentPanel;
