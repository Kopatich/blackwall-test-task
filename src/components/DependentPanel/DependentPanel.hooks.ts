
import React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'store/constants';
import { FILTER_TYPES, getFilterTypeByCode } from './DependentPanel.const';

export const usePanel = () => {
    const [selectedFilter, setselectedFilter] = React.useState<FILTER_TYPES>(FILTER_TYPES.ALL);

    const selectedMainCurrency = useSelector((state: AppState) => state.model.selectedCurrency);
    const filterFromTo = useSelector((state: AppState) => state.data.filter)
        .filter(({ from }) => from.code === selectedMainCurrency)
        .at(0);

    const mainSelectedFilter = useSelector((state: AppState) => state.model.selectedFilter);

    const filterDependentNames = {
        [FILTER_TYPES.CRYPTOCASH]: {
            name: 'Криптовалюты',
            shortnames: [] as string[],
            rank: 3
        },
        [FILTER_TYPES.BANK_RUB]: {
            name: 'Банки RUB',
            shortnames: [] as string[],
            rank: 1
        },
        [FILTER_TYPES.CASH]: {
            name: 'Наличные',
            shortnames: [] as string[],
            rank: 2
        },
        [FILTER_TYPES.ALL]: {
            name: 'Все',
            shortnames: [] as string[],
            rank: 0
        }
    };

    filterFromTo?.to.forEach(({ code }) => {
        const type = getFilterTypeByCode(code);
        filterDependentNames[type].shortnames = [...filterDependentNames[type].shortnames, code];
        filterDependentNames[FILTER_TYPES.ALL].shortnames = [...filterDependentNames[FILTER_TYPES.ALL].shortnames, code];
    });

    console.log(filterDependentNames);

    const filterNames = Object.entries(filterDependentNames)
        .sort(([, filterInfo1], [, filterInfo2]) => filterInfo1.rank - filterInfo2.rank);

    React.useEffect(() => {
        if (selectedFilter !== FILTER_TYPES.ALL) {
            setselectedFilter(FILTER_TYPES.ALL);
        }
    }, [mainSelectedFilter]);

    return { selectedFilter, setselectedFilter, filterNames };
};
