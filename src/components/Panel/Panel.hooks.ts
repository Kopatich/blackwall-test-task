import { mockData } from 'mocks/data';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchDataSuccessAction } from 'store/actions/fetchData';
import { AppState } from 'store/constants';
import { PageLoadState } from 'types/pageLoadState';
import { FILTER_TYPES, filterInfo } from './Panel.const';

export const usePanel = () => {
    const dispatch = useDispatch();
    const [selectedFilter, setselectedFilter] = React.useState<FILTER_TYPES>(FILTER_TYPES.ALL);
    const filterNames = Object.entries(filterInfo).sort(([, filterInfo1], [, filterInfo2]) => filterInfo1.rank - filterInfo2.rank);
    const { state } = useSelector((state: AppState) => state.model);

    React.useEffect(() => {
        if (state === PageLoadState.INIT) {
            dispatch(fetchDataSuccessAction(mockData));
        }
    }, []);

    return { selectedFilter, setselectedFilter, filterNames };
};
