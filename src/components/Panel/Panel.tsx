import Dropdown from 'components/Dropdown/Dropdown';
import React from 'react';
import { useDispatch } from 'react-redux';
import { changeSelectedCurrencyAction, changeSelectedFilterAction } from 'store/actions/changeSelectedFilter';
import styled, { css } from 'styled-components';
import { FILTER_TYPES } from './Panel.const';
import { usePanel } from './Panel.hooks';

const StyledPanel = styled.div`
    border: 1px solid red;
    width: 500px;

    display: flex;
    flex-direction: column;
`;

const StyledPanelHeader = styled.h3`
    color: rgb(184, 81, 81);
`;

const StyledFiltersContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
`;

interface StyledFilterProps {
    isSelected: boolean
}

const StyledFilter = styled.div <StyledFilterProps>`
    padding: 10px;
    border-radius: 10px;
    font-weight: bold;
    cursor: pointer;
    transition: 0.2s;
    
    &:hover {
        background-color: rgba(184, 81, 81, 0.4);
    }

    ${({ isSelected }) => {
    if (isSelected) {
            return css`
                color: white;
                background-color: rgb(184, 81, 81);

                &:hover {
                    background-color: rgb(184, 81, 81);
                }
            `;
        }
    }}
`;

const Panel = () => {
    const dispatch = useDispatch();
    const { selectedFilter, filterNames, setselectedFilter } = usePanel();

    return (
        <StyledPanel>
            <StyledPanelHeader>Отдаете</StyledPanelHeader>
            <StyledFiltersContainer>
                {filterNames.map(([filterCode, filterInfo]) => (
                    <StyledFilter
                        isSelected={selectedFilter === filterCode}
                        key={filterCode}
                        onClick={() => {
                            dispatch(changeSelectedFilterAction(filterCode as FILTER_TYPES));
                            setselectedFilter(filterCode as FILTER_TYPES);
                            dispatch(changeSelectedCurrencyAction(filterInfo.shortnames[0]));
                        }}
                    >
                        {filterInfo.name}
                    </StyledFilter>
                ))}
            </StyledFiltersContainer>
            <Dropdown selectedFilter={selectedFilter} />
        </StyledPanel>
    );
};

export default Panel;
