export enum FILTER_TYPES {
    ALL = 'ALL',
    CRYPTOCASH = 'CRYPTOCASH',
    CASH = 'CASH',
    BANK_RUB = 'BANK_RUB'
}

export const filterInfo: Record<FILTER_TYPES, { name: string, shortnames: string[], rank: number }> = {
    [FILTER_TYPES.CRYPTOCASH]: {
        name: 'Криптовалюты',
        shortnames: ['BTC', 'ETH', 'USDTTRC'],
        rank: 3
    },
    [FILTER_TYPES.BANK_RUB]: {
        name: 'Банки RUB',
        shortnames: ['ACRUB', 'SBERRUB', 'TCSBRUB'],
        rank: 1
    },
    [FILTER_TYPES.CASH]: {
        name: 'Наличные',
        shortnames: ['CASHUSD', 'CASHRUB'],
        rank: 2
    },
    [FILTER_TYPES.ALL]: {
        name: 'Все',
        shortnames: ['BTC', 'ETH', 'USDTTRC', 'ACRUB', 'SBERRUB', 'TCSBRUB', 'CASHUSD', 'CASHRUB'],
        rank: 0
    }
};
