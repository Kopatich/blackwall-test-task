import React from 'react';
import styled from 'styled-components';
import { FILTER_TYPES } from 'components/Panel/Panel.const';
import { useDropdown } from './DependentDropdown.hooks';

const StyledContainer = styled.div`
    display: flex;
    flex-direction: row;
    padding-top: 10px;
`;

const StyledInput = styled.input`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    flex-grow: 3;
    font-size: 16pt;
`;

const StyledSelect = styled.select`
    display: flex;
    flex-grow: 1;
`;

interface DropdownProps {
    selectedFilter: FILTER_TYPES
    filterNames: Array<[string, {
            name: string
            shortnames: string[]
            rank: number
        }]>
}

const Dropdown: React.FC<DropdownProps> = ({ selectedFilter, filterNames }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { directions, filter, state, shortnames } = useDropdown({ selectedFilter, filterNames });

    return (
        <StyledContainer>
            <StyledInput type="number" min={0} step={0.5} defaultValue={0} />
            <StyledSelect defaultValue={shortnames[0]}>
                {shortnames.map((shortname, index) => (
                    <option
                        key={index}
                        value={shortname}
                    >
                        {shortname}
                    </option>
                ))}
            </StyledSelect>
        </StyledContainer>
    );
};

export default Dropdown;
