import { FILTER_TYPES } from 'components/Panel/Panel.const';
import { useSelector } from 'react-redux';
import { AppState } from 'store/constants';

interface useDropdownProps {
    selectedFilter: FILTER_TYPES
    filterNames: Array<[string, {
        name: string
        shortnames: string[]
        rank: number
    }]>
}

export const useDropdown = ({ selectedFilter, filterNames }: useDropdownProps) => {
    const { directions, filter } = useSelector((state: AppState) => state.data);
    const { state } = useSelector((state: AppState) => state.model);

    const shortnames = Object.fromEntries(filterNames)[selectedFilter].shortnames;

    return { directions, filter, state, shortnames };
};
