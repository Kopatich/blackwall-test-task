import React from 'react';
import styled from 'styled-components';
import { FILTER_TYPES } from 'components/Panel/Panel.const';
import { useDropdown } from './Dropdown.hooks';
import { useDispatch } from 'react-redux';
import { changeSelectedCurrencyAction } from 'store/actions/changeSelectedFilter';

const StyledContainer = styled.div`
    display: flex;
    flex-direction: row;
    padding-top: 10px;
`;

const StyledInput = styled.input`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    flex-grow: 3;
    font-size: 16pt;
`;

const StyledSelect = styled.select`
    display: flex;
    flex-grow: 1;
`;

interface DropdownProps {
    selectedFilter: FILTER_TYPES
}

const Dropdown: React.FC<DropdownProps> = ({ selectedFilter }) => {
    const dispatch = useDispatch();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { directions, filter, state, shortnames } = useDropdown({ selectedFilter });

    return (
        <StyledContainer>
            <StyledInput type="number" min={0} step={0.5} defaultValue={0} />
            <StyledSelect
                defaultValue={shortnames[0]}
                onChange={(e) => dispatch(changeSelectedCurrencyAction(e.target.value))}
            >
                {shortnames.map((shortname, index) => (
                    <option
                        key={index}
                        value={shortname}
                    >
                        {shortname}
                    </option>
                ))}
            </StyledSelect>
        </StyledContainer>
    );
};

export default Dropdown;
