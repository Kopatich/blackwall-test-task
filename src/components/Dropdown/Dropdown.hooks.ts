import { filterInfo, FILTER_TYPES } from 'components/Panel/Panel.const';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changeSelectedCurrencyAction } from 'store/actions/changeSelectedFilter';
import { AppState } from 'store/constants';

interface useDropdownProps {
    selectedFilter: FILTER_TYPES
}

export const useDropdown = ({ selectedFilter }: useDropdownProps) => {
    const dispatch = useDispatch();
    const { directions, filter } = useSelector((state: AppState) => state.data);
    const { state } = useSelector((state: AppState) => state.model);

    const { shortnames } = filterInfo[selectedFilter];

    React.useEffect(() => {
        dispatch(changeSelectedCurrencyAction(shortnames[0]));
    }, []);

    return { directions, filter, state, shortnames };
};
