export enum PageLoadState {
    INIT = 'INIT',
    LOADING = 'LOADING',
    IDLE = 'IDLE',
    ERROR = 'ERROR'
};
