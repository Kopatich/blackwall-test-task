import { createAction, createEmptyAction } from 'tools/createAction';
import { Filter, Direction } from 'mocks/data';
import { AnyAction } from '@reduxjs/toolkit';

export interface FetchDataStartActionType extends AnyAction {
    type: 'FETCH_DATA_START'
}

export const fetchDataStartAction = () => createEmptyAction('FETCH_DATA_START');

export interface FetchDataSuccessActionType extends AnyAction {
    type: 'FETCH_DATA_SUCCESS'
    payload: {
        directions: Direction[]
        filter: Filter[]
    }
}

export const fetchDataSuccessAction = (payload: FetchDataSuccessActionType['payload']) => createAction('FETCH_DATA_SUCCESS', payload);

export interface FetchDataErrorActionType extends AnyAction {
    type: 'FETCH_DATA_ERROR'
}

export const fetchDataErrorAction = () => createEmptyAction('FETCH_DATA_ERROR');

export type FetchDataActionsTypes = FetchDataStartActionType | FetchDataSuccessActionType | FetchDataErrorActionType;
