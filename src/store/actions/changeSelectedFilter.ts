import { AnyAction } from '@reduxjs/toolkit';
import { FILTER_TYPES } from 'components/Panel/Panel.const';
import { createAction } from 'tools/createAction';

export interface changeSelectedFilterActionType extends AnyAction {
    type: 'CHANGE_FILTER'
    payload: {
        filter: FILTER_TYPES
    }
}

export const changeSelectedFilterAction = (filter: FILTER_TYPES) => createAction('CHANGE_FILTER', { filter });

export interface changeSelectedCurrencyActionType extends AnyAction {
    type: 'CHANGE_CURRENCY'
    payload: {
        currency: FILTER_TYPES
    }
}

export const changeSelectedCurrencyAction = (currency: string) => createAction('CHANGE_CURRENCY', { currency });

export type changeSelectedFilterActionsTypes = changeSelectedFilterActionType | changeSelectedCurrencyActionType;
