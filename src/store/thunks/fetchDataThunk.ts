// import { Dispatch } from '@reduxjs/toolkit';
import { mockData } from 'mocks/data';
import { AppThunk } from 'store/constants';
import { fetchDataErrorAction, fetchDataStartAction, fetchDataSuccessAction } from 'store/actions/fetchData';
// import { AppThunk } from 'store/constants';

const sleep = async (ms: number) => {
    return await new Promise(resolve => setTimeout(resolve, ms));
};

export const fetchDataThunk = (): AppThunk => (dispatch) => {
    dispatch(fetchDataStartAction());

    const { directions, filter } = mockData;

    sleep(1500).then(() => {
        dispatch(fetchDataSuccessAction({ directions, filter }));
    }, () => {
        dispatch(fetchDataErrorAction());
    });
};
