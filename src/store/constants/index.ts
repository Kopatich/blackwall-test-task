import { ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import { FILTER_TYPES } from 'components/Panel/Panel.const';
import { Direction, Filter } from 'mocks/data';
import { useDispatch } from 'react-redux';
import { changeSelectedFilterActionsTypes } from 'store/actions/changeSelectedFilter';
import { FetchDataActionsTypes } from 'store/actions/fetchData';
import { PageLoadState } from 'types/pageLoadState';

export const initialState = {
    data: {
        directions: [] as Direction[],
        filter: [] as Filter[]
    },
    model: {
        state: PageLoadState.INIT,
        selectedFilter: FILTER_TYPES.ALL,
        selectedCurrency: ''
    }
};

export type AppState = typeof initialState;

export type AppBaseAction = FetchDataActionsTypes | changeSelectedFilterActionsTypes;

export type AppThunk = ThunkAction<void, AppState, unknown, AppBaseAction>;

export type AppAction = AppBaseAction | AppThunk;

export type AppDispatch = ThunkDispatch<AppState, any, AppBaseAction>;

// export type AppDispatch = (action: AppAction) => AppBaseAction;

// export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch;
