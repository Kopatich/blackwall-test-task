import { combineReducers } from '@reduxjs/toolkit';
import { fetchDataReducer } from './data/fetchData';
import { fetchDataModelReducer } from './model/fetchData';

export const reducers = combineReducers({ data: fetchDataReducer, model: fetchDataModelReducer });
