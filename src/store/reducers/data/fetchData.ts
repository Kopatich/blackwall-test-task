import { AppBaseAction, initialState } from 'store/constants';

export const fetchDataReducer = (state = initialState.data, action: AppBaseAction) => {
    switch (action.type) {
        case 'FETCH_DATA_SUCCESS': {
            const { directions, filter } = action.payload;

            return { ...state, directions, filter };
        }

        default: {
            return state;
        }
    }
};
