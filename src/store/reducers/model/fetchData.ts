import { AppBaseAction, initialState } from 'store/constants';
import { PageLoadState } from 'types/pageLoadState';

export const fetchDataModelReducer = (state = initialState.model, action: AppBaseAction) => {
    switch (action.type) {
        case 'FETCH_DATA_START': {
            return { ...state, state: PageLoadState.LOADING };
        }

        case 'FETCH_DATA_SUCCESS': {
            return { ...state, state: PageLoadState.IDLE };
        }

        case 'FETCH_DATA_ERROR': {
            return { ...state, state: PageLoadState.ERROR };
        }

        case 'CHANGE_FILTER': {
            return { ...state, selectedFilter: action.payload.filter };
        }

        case 'CHANGE_CURRENCY': {
            return { ...state, selectedCurrency: action.payload.currency };
        }

        default: {
            return state;
        }
    }
};
