import DependentPanel from 'components/DependentPanel/DependentPanel';
import Panel from 'components/Panel/Panel';
import React from 'react';
import './App.css';

const App: React.FC = () => {
  return (
    <>
      <Panel />
      <DependentPanel />
    </>
  );
};

export default App;
